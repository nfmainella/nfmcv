import * as classNames from "classnames";
import * as React from "react";
import './SectionBooster.css';

export interface ISectionBooster {
    classModifier?: string;
    whiteFadeBackground?: boolean;
    paddingTop: number;
    children?: any;
    center?: boolean;
}

export class SectionBooster extends React.Component<ISectionBooster, object> {
    public render() {
        const {classModifier, center = false, children, whiteFadeBackground = false, paddingTop} = this.props;
        return (
            (paddingTop > 0) && <section
                className={classNames('SectionBooster', {'SectionBooster--whiteFadeBackground': whiteFadeBackground}, {'SectionBooster--centerVertically': center}, classModifier)}
                style={{marginTop:paddingTop}}>
                {children}
            </section>
        )
    }
}

export default SectionBooster