import { ICurriculumVitae } from "curriculum-vitae/CurriculumVitae.interface";
import { connect } from "react-redux";
import SectionBooster from "./SectionBoosterComponent";

export function mapStateToProps({ navigationBar }: ICurriculumVitae) {
  return {
    paddingTop: navigationBar.positioning.topPaddingAmount ? navigationBar.positioning.topPaddingAmount : 0
  };
}

export default connect(
  mapStateToProps
)(SectionBooster);
