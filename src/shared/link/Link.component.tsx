import * as React from 'react';

export interface ILinkProps {
    href?: string;
    children?: any;
    shouldTargetNew?: boolean;
}

import './Link.css';

const Link: React.FunctionComponent<ILinkProps> = ({ href, children, shouldTargetNew = false }: ILinkProps) => {
    return (
        <>
            <a href={href} className="Link" target={shouldTargetNew ? '_new' : '_blank'}>{children}</a>
        </>
    )
};

export default Link;