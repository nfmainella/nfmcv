import * as React from "react";
export interface IButtonProps {
    href?: string;
    link?: any;
    target?: string;
    children: any;
    classModifier?: string;
}

import * as classNames from "classnames";
import { NavLink } from "react-router-dom";
import './Buttons.css';

export class Button extends React.Component<IButtonProps> {
    public render() {
        const { target, href, link, children, classModifier } = this.props;
        const classNameModifier = classNames('Button', classModifier)

        return (
            href ? <a className={classNameModifier}
                href={href ? href : '#'}
                target={target ? target : 'default'}>{children}
            </a>
                : <NavLink to={link} className={classNameModifier} exact={true}>
                    {children}
                </NavLink>
        )
    }
}