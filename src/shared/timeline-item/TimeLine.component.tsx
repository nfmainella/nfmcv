import * as React from "react";

export interface ITimelineItem {
    header: any;
    children?: any;
    backgroundImage?: string;
    description?: any;
    rightBubble?: any;
    toggleContent?: boolean;
}

import './TimeLineItem.css';

const TimelineItem: React.FunctionComponent<ITimelineItem> = (props: ITimelineItem) => {
    const timelineItem = React.useRef(null);
    const [toggleContent, setToggleContent] = React.useState(false);


    React.useEffect(() => {
        if (props.toggleContent) {
            setToggleContent(props.toggleContent)
        }
    });


    const maybeToggleContent = () => setToggleContent(!toggleContent);
    return (
        <>
            <div className="TimelineItem" ref={timelineItem}>


                <div className="TimelineItem-content">
                    <div className="TimelineItem-header" onClick={maybeToggleContent}>{props.header}</div>
                    {toggleContent && props.children}

                    <div className="TimelineItem-description">
                        {props.description}
                    </div>
                </div>
                {props.rightBubble &&
                    <div className="TimelineItem-rightBubble">
                        {props.rightBubble}
                    </div>
                }
            </div>
        </>
    );
}

export default TimelineItem