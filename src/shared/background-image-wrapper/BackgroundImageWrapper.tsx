import * as classNames from "classnames";
import * as React from "react";
import "./BackgroundImage.css";

export interface IBackgroundImage {
    classModifier?: string;
    imageUrl?: string;
    fullHeight?: boolean;
    height?: number;
    fullWidth?: boolean;
    noRepeat?: boolean;
}

export class BackgroundImageWrapper extends React.Component<IBackgroundImage> {
    public render() {
        const {classModifier, imageUrl, children, height, fullHeight=false, fullWidth=false, noRepeat = false} = this.props;
        const background = `linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(${imageUrl}) ${noRepeat && 'no-repeat'}`;
        return (
        <div className={classNames('BackgroundImage', {'BackgroundImage--fullHeight': fullHeight}, {'BackgroundImage--fullWidth': fullWidth}, classModifier)} style={{background, height: `${height}px`,}}>
            {children}
        </div>
       )
    }
}

export default BackgroundImageWrapper