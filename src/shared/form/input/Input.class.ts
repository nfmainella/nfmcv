

export class Input {
    private classModifier?: string;
    private name?: string;
    private value?: string;
    private label?: string;
    private onBlur?: any;
    private onClick?: any;
    private onChange?: any;
    private validation: IInputValidation;
    constructor(private name?: string, private value?:string, ) {
        setVal
    }
}

export interface IInputValidation {
    isValid?: boolean;
    name?: string;
    value?: any;
    type?: string;
    required?: boolean;
    maxLength?: number;
    minLength?: number;
    max?: number;
    min?: number;
    errorMessage?: string;
}

export class InputLabel {
    public top: boolean;
    constructor(top?: boolean) {
        top ? this.top = top : this.top = false;
    }
}

