import * as classNames from "classnames";
import * as React from "react";
import './TextInput.css';
import {ITextInput, ITextInputLabel} from "./TextInput.interface";

function shouldRunFunctions(onClickFn: any, extra?: any) {
    if (extra){
        extra()
    }
    if (onClickFn) {
        onClickFn()
    }
}

function toggleInputLabelPosition (setInputLabel: any, inputLabel: any) {
    setInputLabel({...setInputLabel, top: !inputLabel.top})
}

const TextInput: React.FunctionComponent<ITextInput> = (props: ITextInput) => {
    const [inputLabel, setInputLabel] = React.useState(new ITextInputLabel())

    const inputProps = {
        className: classNames("TextInput-input", props.classModifier),
        name: props.name,
        onBlur: () => shouldRunFunctions(toggleInputLabelPosition(setInputLabel, inputLabel), props.onBlur),
        onChange: props.onChange,
        onClick: () => shouldRunFunctions(toggleInputLabelPosition(setInputLabel, inputLabel), props.onClick),
        value: props.value,
    }

    return (
        <div className="TextInput-wrapper">
        {
            props.label &&
                <label className={classNames('TextInput-label', {'TextInput-label--top' : inputLabel.top})}>
                    {props.label}
                </label>
        }

        <div className="TextInput-inputWrapper">
            <input type="text" {...inputProps}/>    
        </div>

        {props.errorMessage  &&
        <div className="TextInput-error">
            {props.errorMessage}
        </div>
        }
        </div>
    )
}

export default TextInput