import * as classNames from "classnames";
import * as React from "react";
import './SectionBooster.css';

export interface ITextInput {
    classModifier?: string;
    name?: string;
    value: number;
    onBlur?: any;
    onChange?: any;
}

const TextInput: React.FunctionComponent<ITextInput> = (props: ITextInput) => {
    return (
        <input type="text" value={props.value}
               name={props.name}
               onBlur={props.onBlur}
               onChange={props.onChange}
               className={classNames("TextInput", props.classModifier)}/>
    )
}

export default TextInput