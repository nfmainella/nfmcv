import { EducationalExperience } from "container/educational-experience/EducationalExperience.interface";
import * as React from "react";
import "./EducationalExperienceItem.css";

export interface IEducationalExperienceItemProps {
  educationalExperience: EducationalExperience;
}

class EducationalExperienceItem extends React.Component<
  IEducationalExperienceItemProps,
  object
> {
  public render() {
    const { educationalExperience } = this.props;
    return (
      <div className="educational-experience-item">
        <p className="firstLine">
          <strong className="company">
            {educationalExperience.institution}
          </strong>
          <span className="location">Rosario, Argentina</span>
        </p>
        <div
          className="description"
          dangerouslySetInnerHTML={createDescription(
            educationalExperience.description
          )}
        />
      </div>
    );
  }
}

export default EducationalExperienceItem;

function createDescription(description: string) {
  return { __html: "<p>" + description + "</p>" };
}
