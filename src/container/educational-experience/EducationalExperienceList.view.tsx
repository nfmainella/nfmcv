import * as React from "react";
import EducationalExperienceItem from "./educational-experience-item/EducationalExperienceItem";
import { EducationalExperience } from "./EducationalExperience.interface";

export interface IEducationalExperienceListProps {
  elementList: EducationalExperience[];  
  showAll?: () => void;
  showGraduated?: () => void;
}

function EducationalExperienceListView({ elementList }: IEducationalExperienceListProps) {
    return (    
      <div>
        <h4 className="block-header">Educational Experience</h4>      
        {elementList.map(
          (element: EducationalExperience) => (
        <EducationalExperienceItem educationalExperience={element} />
      ))}
      </div>
    );
}

export default EducationalExperienceListView