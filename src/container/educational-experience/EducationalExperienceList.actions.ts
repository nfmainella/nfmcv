
import * as constants from './EducationalExperience.constants';

export interface IShowAll {
    type: constants.SHOW_ALL;
}

export interface IShowGraduated {
    type: constants.SHOW_GRADUATED;
}

export type EducationalExperienceListAction = IShowAll | IShowGraduated;

export function showAll(): IShowAll {
    return {
        type: constants.SHOW_ALL
    };
}

export function showGraduated(): IShowGraduated {
    return {
        type: constants.SHOW_GRADUATED
    };
}