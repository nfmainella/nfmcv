export class EducationalExperience {
    public institution: string;
    public startYear: number;
    public endYear: number;    
    public graduated: boolean = false;    
    public description: string;

	constructor(institution: string, startYear: number, endYear: number, graduated: boolean, description: string) {
        this.institution = institution;
        this.startYear = startYear;
        this.endYear = endYear;
        this.graduated = graduated;
        this.description = description;
	}

}
