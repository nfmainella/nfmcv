import { EducationalExperience } from "./EducationalExperience.interface";

export interface IEducationalExperienceListState {
    elementList: EducationalExperience[];
}