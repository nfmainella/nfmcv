import {IEducationalExperienceListState} from "./educational-experience/EducationalExperienceListState.interface";
import { IContactInfoState } from "./home/contact-info/ContactInfoState.interface";
import {IWorkExperienceListState} from "./work-experience-list/WorkExperienceListState.interface";

export interface IContainer {
    educationalExperience: IEducationalExperienceListState;
    workExperience: IWorkExperienceListState;
    contactInfo: IContactInfoState;
}