import { emptyValueIfUndefined } from 'utils/classUtils';

export class WorkExperienceMedia {
    public id: number;
    public description: string;
    public url: string;
    public isVideo: boolean;

    constructor(id?: number, description?: string, url?: string, isVideo?: boolean) {
        this.id = emptyValueIfUndefined(id, 0);
        this.description = emptyValueIfUndefined(description);
        this.url = emptyValueIfUndefined(url)
        this.isVideo = emptyValueIfUndefined(isVideo, false)
    }
}