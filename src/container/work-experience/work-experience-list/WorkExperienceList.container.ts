import { connect } from "react-redux";
import { Dispatch } from "redux";
import {showAll, showCurrent, WorkExperienceListAction} from "./WorkExperienceList.actions";

import { ICurriculumVitae } from "curriculum-vitae/CurriculumVitae.interface";
import WorkExperienceListView from "./WorkExperienceList.view";

export function mapStateToProps({ container }: ICurriculumVitae) {
  return {
    elementList: container.workExperience.elementList
  };
}

export function mapDispatchToProps(
  dispatch: Dispatch<WorkExperienceListAction>
) {
  return {
    showAll: () => dispatch(showAll()),
    showCurrent: () => dispatch(showCurrent())
  };
}


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WorkExperienceListView);
