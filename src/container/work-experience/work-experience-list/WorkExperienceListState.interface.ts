import { WorkExperience } from "./WorkExperience.interface";

export interface IWorkExperienceListState {
    elementList: WorkExperience[];
}