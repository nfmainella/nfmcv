import * as React from 'react';
import SectionBooster from 'shared/section-booster/SectionBooster.container';
import WorkExperienceListView from './WorkExperienceList.container';

export class WorkExperienceList extends React.Component {
  
 public render() {
    return (
        <SectionBooster center={true}>
          <WorkExperienceListView />
        </SectionBooster>
    )  
  }
}

export default WorkExperienceList;

