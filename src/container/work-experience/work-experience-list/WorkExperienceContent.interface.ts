import { emptyValueIfUndefined } from 'utils/classUtils';

export class WorkExperienceDescription {
    public id: number;
    public description: string;
    public jobTasks: string[];
    public jobPictures: string[];
    public backgroundImage: string;
    public url: string;

    constructor(id?: number, description?: string, jobTasks?: string[], jobPictures?: string[], backgroundImage?: string, url?: string) {
        this.id = emptyValueIfUndefined(id, 0);
        this.description = emptyValueIfUndefined(description);
        this.jobTasks = emptyValueIfUndefined(jobTasks, []);
        this.jobPictures = emptyValueIfUndefined(jobPictures, []);
        this.backgroundImage = emptyValueIfUndefined(backgroundImage);
        this.url = emptyValueIfUndefined(url)
    }
}