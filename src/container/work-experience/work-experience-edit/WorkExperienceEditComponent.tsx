import { WorkExperience } from "container/work-experience-list/WorkExperience.interface";
import { findWorkExperience } from "container/work-experience-list/WorkExperience.state";
import * as React from "react";
import { NavLink, useParams } from "react-router-dom";
import BackgroundImageWrapper from "shared/background-image-wrapper/BackgroundImageWrapper";
import SectionBooster from 'shared/section-booster/SectionBooster.container';
import { getDateDistance, getDatePeriod } from "utils/dateUtils";

import './WorkExperienceEdit.css';

function WorkExperienceEditView() {
  const { id } = useParams();
  const [workExperience, setWorkExperience] = React.useState(new WorkExperience())

  React.useEffect(() => {
    if (id && !workExperience.initialized) {
      setWorkExperience({ ...findWorkExperience(id), initialized: true })
    }
  })

  return (
    <SectionBooster>
      {workExperience.initialized ?
        <div className="WorkExperienceEdit">
          <BackgroundImageWrapper
            imageUrl={workExperience.workExperienceDescription.backgroundImage}
            fullWidth={true} noRepeat={true}>
            <div className="WorkExperienceEdit-header">
              <NavLink className="WorkExperienceEdit-breadCrumb" to={`/work_experience`}>
                ← Back to all of my work experiences
              </NavLink>
              <p className="WorkExperienceEdit-company no-margin tt-u">{workExperience.company}</p>
              <div className="WorkExperienceEdit-periodLength no-margin hidden-sm">For {getDateDistance(workExperience.yearPeriod)} {getDatePeriod(workExperience.yearPeriod)}</div>
              <div className="WorkExperienceEdit-locationWrapper hidden-sm">
                <span className="WorkExperienceEdit-location tt-u">{workExperience.remote ? 'Remote' : `${workExperience.city}, ${workExperience.country}`}</span> <span className="WorkExperienceEdit tt-u">{workExperience.position}</span>
              </div>

              <div className="WorkExperienceEdit-locationWrapper sm-only">
                <span className="WorkExperienceEdit-location tt-u">{workExperience.remote ? 'Remote' : `${workExperience.city}, ${workExperience.country}`}</span> 
                  <span className="WorkExperienceEdit tt-u">{workExperience.position}</span>
              </div>
            </div>
          </BackgroundImageWrapper>
          <div className="WorkExperienceEdit-descriptionWrapper">
            <div className="WorkExperienceEdit-imageWrapper">
              <img src={workExperience.thumbnailImage} className="WorkExperienceEdit-image" />
            </div>
            <h1 className="WorkExperienceEdit-descriptionHeader">About the product  / company</h1>
            <div className="WorkExperienceEdit-description">
              {workExperience.workExperienceDescription.description}
            </div>

            <h1 className="WorkExperienceEdit-descriptionHeader">What i did in my work</h1>
            {workExperience.workExperienceDescription.jobTasks.map(
              task =>
                <div className="WorkExperienceEdit-jobTask" key={task}>
                  {task}
                </div>
            )}
          </div>

        </div>
        : 'Loading'}
    </SectionBooster>
  )
}

export default WorkExperienceEditView
