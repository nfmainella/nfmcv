import {ContactInfoActions} from "./ContactInfo.actions";
import {SHOW_ALL} from "./ContactInfo.constants";
import {socialIconsList} from "./ContactInfo.state";
import {IContactInfoState} from "./ContactInfoState.interface";

export function contactInfoReducer (state: IContactInfoState = {'socialIcons': socialIconsList}, action: ContactInfoActions): IContactInfoState {
    switch (action.type) {
        case SHOW_ALL:
            return state;
        default:
            return state;
    }
}