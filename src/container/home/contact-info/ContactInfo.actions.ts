
import * as constants from './ContactInfo.constants';

export interface IShowAll {
    type: constants.SHOW_ALL;
}

export type ContactInfoActions = IShowAll;

export function showAll(): IShowAll {
    return {
        type: constants.SHOW_ALL
    };
}