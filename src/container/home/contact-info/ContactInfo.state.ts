import {SocialIcon} from "shared/social-icon/SocialIcon.interface";
const socialIconPrefix = 'nfm-social-icons';

export const socialIconsList: SocialIcon[] = [
    new SocialIcon(`${socialIconPrefix} bitBucket`, 'https://bitbucket.org/nfmainella/'),
    new SocialIcon(`${socialIconPrefix} gitHub`, 'https://github.com/nmainella'),
    new SocialIcon(`${socialIconPrefix} skype`, 'skype:nfmainella'),
    new SocialIcon(`${socialIconPrefix} linkedIn`, 'https://www.linkedin.com/in/nicolasfmainella/'),
    new SocialIcon(`${socialIconPrefix} stackOverflow`, 'https://stackoverflow.com/users/2804523/nicolas-franco-mainella')
];