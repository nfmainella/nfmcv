import {Input} from "shared/form/input/Input.class";

export class ContactFormComponent {
    // tslint:disable-next-line:variable-name
    private _fields: Input[];

    get fields(): Input[] {
        return this._fields;
    }

    set fields(value: Input[]) {
        this._fields = value;
    }

    constructor(contactForm: Input[]) {
        this._fields = contactForm;
    }
}