import * as React from 'react';

import { BackgroundImageWrapper } from "shared/background-image-wrapper/BackgroundImageWrapper";
import { Button } from 'shared/buttons/Buttons.component';
import Link from 'shared/link/Link.component'
import SectionBooster from 'shared/section-booster/SectionBooster.container'
import './AboutMe.css';

class AboutMe extends React.Component {
    public render() {
        return <SectionBooster>
                    <BackgroundImageWrapper
                        imageUrl="https://firebasestorage.googleapis.com/v0/b/nfm-cv.appspot.com/o/home-new-medium.jpg?alt=media&token=393c9d90-87bc-45f0-8051-d148f8b3fb6c"
                        fullWidth={true} noRepeat={true} classModifier="BackgroundImage--smallPadding">
                        <div className="AboutMe-pageHeader">
                            <div className="AboutMe-profilePicture"/>
                        </div>
                    </BackgroundImageWrapper>
                    <div className="AboutMe">
                        <div className="AboutMe-personalIntroduction">
                            <p className="Paragraph Paragraph--medium Paragraph--identSmall">
                                I am an ambitious, high energy <strong>front end developer</strong>, with a degree in System's
                                Engineering, located in <strong>Rosario,
                                Argentina</strong> who loves and takes great pride in his work, and is always hungry to
                                improve. I work remotely, but frequent <Link href="http://www.workaround.com.ar"
                                                                             shouldTargetNew={true}>WorkAround co-working
                                space</Link><br/><br/>
                                Caffeine aficionado, frequent traveler, light gamer, and fitness enthusiast with a <i>Mens sana
                                in corpore sano</i> mindset who also loves cooking for the people i care and to have the
                                occasional locally brewed artisan beer, or two.<br/><br/>
                                Professionally, the front end is where my focus is at, with React, TypeScript, Babel and Webpack
                                as my weapons of choice, with a few splashes of Python and NodeJS for the backend.
                            </p>
                        </div>
                        <div className="AboutMe-externalLinks mb-2">
                            <Button  href="https://www.linkedin.com/in/nicolasfmainella/">
                                Linkedin <span className="AboutMe-icon AboutMe-icon--LinkedIn" />
                            </Button>

                            <Button href="https://docs.google.com/document/d/1XBcwMJvl-ieIm8fMEQzIienuL4NdFqSK3_ukXDO0d5U/edit?usp=sharing" classModifier="Button--white ml-2">
                                Download my CV <span className="AboutMe-icon AboutMe-icon--Download" />
                            </Button>
                        </div>
                       
                    </div>
        </SectionBooster>;
    }
}

export default AboutMe;
