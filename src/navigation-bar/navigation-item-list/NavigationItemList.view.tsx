import classNames from 'classnames'
import {NavigationItemClass} from "navigation-bar/navigation-item-list/navigation-item/NavigationItem.class";
import * as React from "react";
import NavigationToggle from "../navigation-toggle/NavigationToggle";
import NavigationItem from "./navigation-item/NavigationItem";
import './NavigationItemList.css';

export interface IMenuListProps {
    itemList: NavigationItemClass[];
    showNavBar: boolean;
    setShowNavBar?: any;
    topNavBarPosition?: number;
}

const NavigationItemList: React.FunctionComponent<IMenuListProps> = ({itemList, showNavBar, setShowNavBar, topNavBarPosition}: IMenuListProps) => {
    const navigationItemList = React.useRef(null);
    const [paddingTopAmount, setPaddingTopAmount] = React.useState(0);
    const [elementHeight, setElementHeight] = React.useState(0);

    React.useEffect(() => {
        function getAttributes(element: any) {
            const elementBoundClient = element.getBoundingClientRect();
            if (!showNavBar) {
                setPaddingTopAmount(elementBoundClient.height + topNavBarPosition);
            }
            setElementHeight(elementBoundClient.height)
        }

        if (navigationItemList.current) {
            getAttributes(navigationItemList.current)
        }
    });

    function toggleNavBar() {
        setShowNavBar(!showNavBar)
    }

    const mappedItemList = itemList.map(
        (element: NavigationItemClass) => <NavigationItem item={element} key={element.path} onClick={toggleNavBar}/>
    );

    return (
        <>
            <div className="NavigationItemList">
                <div className="NavigationItemList-wrapper">
                    <div className="NavigationItemList-mobileHeader md-down">
                        <NavigationToggle onClick={toggleNavBar} showNavBar={showNavBar} />
                        <h1 className="NavigationItemList-title">Nicolás Franco Mainella</h1>
                    </div>

                    <div className="NavigationItemList-elementList lg-only tt-u">
                        {mappedItemList}
                    </div>
                </div>
            </div>
            <div
                className={classNames('NavigationItemList-elementListWrapper md-down', {'NavigationItemList-elementListWrapper--visibilityAnimationPre': !showNavBar})}
                style={showNavBar ? {height: `${elementHeight}px`} : {}}>
                <div ref={navigationItemList}
                     className={classNames('NavigationItemList-elementList', {'NavigationItemList-elementList--visibilityAnimationPost': showNavBar}, {'NavigationItemList-elementList--visibilityAnimationPre': !showNavBar})}
                     style={showNavBar ? {} : {transform: `translateY(-${paddingTopAmount}px)`}}>
                    {mappedItemList}
                </div>
            </div>

        </>
    )
};

export default NavigationItemList;