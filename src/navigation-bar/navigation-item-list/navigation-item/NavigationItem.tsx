import * as classNames from 'classnames';
import * as React from 'react';
import {NavLink} from 'react-router-dom';
import {NavigationItemClass} from './NavigationItem.class';
import './NavigationItem.css';

export interface INavigationItemProps {
    item: NavigationItemClass;
    classModifier?: string;
    onClick?: any;
}

export default class NavigationItem extends React.Component<INavigationItemProps, object> {
    public wrapInNavLink(children: any, path: string, onClick?: any) {
        return <NavLink to={path} className="NavigationItem NavigationItem--inactive"
                             activeClassName="NavigationItem--active" exact={true} onClick={onClick}>{children}</NavLink>
    };

    public content(title: string, classModifier?: string) {
        return <div className={classNames('NavigationItem-content', {classModifier})}><h5
            className="NavigationItem-title no-margin">{title}</h5></div>
    };

    public render() {
        const {item, classModifier, onClick} = this.props;

        return (
            item.path ?
                this.wrapInNavLink(this.content(item.title, classModifier), item.path, onClick) :
                this.content(item.title, classModifier)
        )
    }
}