import { NavigationItemClass } from "./navigation-item-list/navigation-item/NavigationItem.class";
import { INavigationBar } from "./NavigationBar.interface";

const menuList: NavigationItemClass[] = [
    new NavigationItemClass('--Home', 'Home', 'Start to know me', '/'),
    new NavigationItemClass('--Person', 'About me', 'More about me', '/about_me'),
    new NavigationItemClass('--BriefCase', 'Work', 'Professional career', '/work_experience'),
    new NavigationItemClass('--College', 'Education', 'Academic career', '/academic_experience'),
    new NavigationItemClass('--Comment', 'Contact', 'Send me a message', '/contact')
  ];

export const iNavigationBarState: INavigationBar = {
  itemList: menuList,
  positioning: {
      show: false,
      topPaddingAmount: 0
  }
}
