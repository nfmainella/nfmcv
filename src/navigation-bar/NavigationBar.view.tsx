import * as React from "react";
import {NavigationItemClass} from './navigation-item-list/navigation-item/NavigationItem.class';
import NavigationItemList from './navigation-item-list/NavigationItemList.view';
import {DEFAULT_TOP_PADDING_AMOUNT} from "./NavigationBar.constants";
import "./NavigationBar.css";

export interface INavigationBarView {
    itemList: NavigationItemClass[];
    setNavBarTopPosition: (paddingTop: number) => void;
}

const NavigationBarView: React.FunctionComponent<INavigationBarView> = (props: INavigationBarView) => {
    const navbarToggle = React.useRef(null);
    const {setNavBarTopPosition, itemList} = props;
    const [showNavBar, setShowNavBar] = React.useState(false);
    const [topNavBarPosition, setTopNavBarPosition] = React.useState(0);

    React.useEffect(() => {
        function getAttributes(element: any) {
            const elementBoundClient = element.getBoundingClientRect();
            const paddingTopAmount = elementBoundClient.height ? elementBoundClient.height : DEFAULT_TOP_PADDING_AMOUNT;
            return {
                paddingTop: ` ${paddingTopAmount}px`,
                paddingTopAmount
            }
        }

        if (navbarToggle.current && !topNavBarPosition  ) {
            setNavBarTopPosition(getAttributes(navbarToggle.current).paddingTopAmount);
            setTopNavBarPosition(getAttributes(navbarToggle.current).paddingTopAmount)
        }
    });


    return (
        <>
            <div className="NavigationBar" ref={navbarToggle}>
                <NavigationItemList itemList={itemList} showNavBar={showNavBar} setShowNavBar={setShowNavBar} topNavBarPosition={topNavBarPosition}/>
            </div>
        </>
    );
};

export default NavigationBarView