
import * as constants from './NavigationBar.constants';

export interface IToggleNavigationBar {
    type: constants.TOGGLE_NAVIGATION_BAR;
}

export function toggleNavigationBar(): IToggleNavigationBar {
    return {
        type: constants.TOGGLE_NAVIGATION_BAR
    };
}
export interface INavBarSetTopPositionPayload {
    topPaddingAmount: number;
}

export interface ISetTopPositionNavigationBar {
    type: constants.SET_TOP_POSITION_NAVIGATION_BAR,
    payload: INavBarSetTopPositionPayload
}

export function setTopPosition(payload: INavBarSetTopPositionPayload):ISetTopPositionNavigationBar {
    return {
        payload,
        type: constants.SET_TOP_POSITION_NAVIGATION_BAR
    }
}


export type NavigationBarAction = IToggleNavigationBar | ISetTopPositionNavigationBar;
