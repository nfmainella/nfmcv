import { NavigationBarAction } from './NavigationBar.actions';
import {SET_TOP_POSITION_NAVIGATION_BAR, TOGGLE_NAVIGATION_BAR} from './NavigationBar.constants'
import { INavigationBar } from './NavigationBar.interface';
import { iNavigationBarState } from './NavigationBar.state';

export function navigationBarReducer(state: INavigationBar = iNavigationBarState, action: NavigationBarAction): INavigationBar {
  switch (action.type) {
    case TOGGLE_NAVIGATION_BAR:
      return {...state, positioning: {...state.positioning, show: !state.positioning.show}}; 
    case SET_TOP_POSITION_NAVIGATION_BAR:
      return {...state, positioning: {...state.positioning, topPaddingAmount: action.payload.topPaddingAmount}};
    default:
      return state;
  }
}