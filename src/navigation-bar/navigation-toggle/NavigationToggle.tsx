import * as classNames from 'classnames';
import * as React from 'react';
import './NavigationToggle.css';

export interface INavigationToggleProps {
    showNavBar?: boolean;
    onClick?: any;
}

export default class NavigationToggle extends React.Component<INavigationToggleProps, object> {
    public render() {
        const {showNavBar, onClick} = this.props;

        return (
            <div className="NavigationToggle" onClick={onClick}>
                <div
                    className={classNames('NavigationToggle-item NavigationToggle-item--first', {'NavigationToggle-item--toggleFirst': showNavBar})}/>
                <div
                    className={classNames('NavigationToggle-item NavigationToggle-item--second', {'NavigationToggle-item--toggleSecond': showNavBar})}/>
            </div>
        )
    }
}