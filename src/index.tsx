import * as React from 'react';
import * as ReactDOM from 'react-dom';

import thunk from 'redux-thunk';
import './base.css';
import './index.css';

import { Provider } from 'react-redux';
import { applyMiddleware, createStore, Store } from 'redux';
import { CurriculumVitaeRouter } from './curriculum-vitae/CurriculumVitae.router';
import { curriculumVitaeState } from './curriculum-vitae/CurriculumVitae.state';
import registerServiceWorker from './registerServiceWorker';

const w : any = window as any;
const devtools: any = w.devToolsExtension ? w.devToolsExtension() : (f:any)=>f;
const middleware = applyMiddleware(thunk);

const store: Store<any> = middleware(devtools(createStore))(curriculumVitaeState);

ReactDOM.render(
  <Provider store={store}>
    <CurriculumVitaeRouter />
  </Provider>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
