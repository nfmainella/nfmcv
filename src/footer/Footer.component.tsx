
import * as React from "react";
import './Footer.css';

export const FooterComponent = () => {
        const currentYear = new Date().getFullYear();
        return (
            <div className="Footer">Copyright © Nicolás Franco Mainella {currentYear}. All Rights Reserved</div>
        )
}