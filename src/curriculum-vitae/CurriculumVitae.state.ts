import { containerReducer } from "container/Container.reducer";
import { navigationBarReducer } from "navigation-bar/NavigationBar.reducer";
import { combineReducers } from "redux";
import { ICurriculumVitae } from "./CurriculumVitae.interface";

export const curriculumVitaeState = combineReducers<ICurriculumVitae>({
    container: containerReducer,
    navigationBar: navigationBarReducer
})