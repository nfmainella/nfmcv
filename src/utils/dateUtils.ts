
import moment from 'moment';
import { YearPeriodWithMonth } from "./YearPeriodWithMonth";
import { YearWithMonth } from "./YearWithMonth";
function serializeDate(date: YearWithMonth) {
    return moment(`${date.month}-1-${date.year}`)
}

function roundNumber(origin: number) {
    return origin - origin % 1
}

function getDateDifference(date: YearPeriodWithMonth) {
    return date.endYear ? moment.duration(serializeDate(date.endYear).diff(serializeDate(date.startYear)))
        : moment.duration(moment().diff(serializeDate(date.startYear)))
}

export function getDateDistance(date?: YearPeriodWithMonth) {
    let dateDifference 
    if (date) {
        dateDifference = getDateDifference(date)
    }
    if (dateDifference) {
        const roundedDateDifferenceAsYears = roundNumber(dateDifference.asYears())
        const roundedDateDifference = roundNumber(dateDifference.asMonths())
        
        if (dateDifference.asYears() > 1) {
            const monthDif = roundedDateDifference - roundedDateDifferenceAsYears * 12
            return monthDif > 0 ? `${roundedDateDifferenceAsYears} year${roundedDateDifferenceAsYears > 1 ? 's' : ''} and ${monthDif} months.` : dateDifference.asYears()
        } else {
            return dateDifference.asMonths() > 1 ? `${roundedDateDifference} months` : `${roundedDateDifference} month`
        }
    } else {
        return 0;
    }
   
}
function formattedMonth(datePeriod: YearWithMonth) {
    console.log(`${datePeriod.year}-1-${datePeriod.month}`)
    return moment(`${datePeriod.year}-${datePeriod.month}-1`, 'YYYY-MM-DD ').format('MMMM')
}

export function getDatePeriod(date?: YearPeriodWithMonth) {
    let retval
    if (date && date.startYear && date.endYear) {
        retval =  `From ${formattedMonth(date.startYear)} of ${date.startYear.year} to ${formattedMonth(date.endYear)} of ${date.endYear.year}`
    } return retval
}