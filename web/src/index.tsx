import * as React from 'react';
import * as ReactDOM from 'react-dom';

import './base.css';
import './index.css';

import { Provider } from 'react-redux';
import {ReCaptchaProvider} from "shared/recaptcha/ReCaptcha.provider";
import { CurriculumVitaeRouter } from './curriculum-vitae/CurriculumVitae.router';
import registerServiceWorker from './registerServiceWorker';

import store from './redux/store/';

// const devtools: any = w.devToolsExtension ? w.devToolsExtension() : (f:any)=>f;

// const store: Store<any> = devtools(createStore)(curriculumVitaeState);

ReactDOM.render(
  <Provider store={store}>
    <ReCaptchaProvider
        reCaptchaKey="6LdaEdsZAAAAAAQUj6eyN9iTzRJL12jfVSVWFPSS"
        scriptProps={{ async: true, id: 'nfm' }}
    >
    <CurriculumVitaeRouter />
    </ReCaptchaProvider>
  </Provider>,
  document.getElementById('root') as HTMLElement
);

registerServiceWorker();
