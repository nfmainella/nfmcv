import {combineReducers} from "redux";

import {IContainer} from "./Container.interface";
import {educationalExperienceListReducer} from "./educational-experience/EducationalExperience.reducer";
import { contactInfoReducer } from "./home/contact-info/ContactInfo.reducer";
import {workExperienceListReducer} from "./work-experience/work-experience-list/WorkExperience.reducer";

export const containerReducer = combineReducers<IContainer>(
    {
        contactInfo: contactInfoReducer,
        educationalExperience: educationalExperienceListReducer,
        workExperience: workExperienceListReducer
    })