import {Input} from "shared/form/input/Input.class";

export class IContactFormComponent {
    public name: Input;
    public email: Input;
    public message: Input;

    constructor(name: Input, email: Input, message: Input) {
        this.name = name;
        this.email = email;
        this.message = message;
    }
}