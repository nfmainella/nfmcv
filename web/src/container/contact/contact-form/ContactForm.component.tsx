import React, {useEffect, useState} from "react";
import Form from "shared/form/form/Form.component";
import { Input } from "shared/form/input/Input.class";
import {initContactForm} from 'utils/contactFormUtils';
import {CONTACT_FORM_SUBMIT_URL} from './ContactForm.constants'

function ContactForm() {
    const [name, setName] = useState<Input>(new Input())
    const [email, setEmail] = useState<Input>(new Input())
    const [message, setMessage] = useState<Input>(new Input())
    const [inputs, setInputs] = useState<Input[]>([])

    const onSubmit = (valid: boolean, validatedFields: Input[]): void => {
        setInputs(validatedFields)
        if (valid) {console.log('Valid')} 
    }
    // Initializing form
    useEffect(
        () => {
            initContactForm(setName, setEmail, setMessage);
        }
    , [])

    useEffect(() => {
        setInputs([name, email, message])   
    }, [email, message, name])
        
   
    return (
        <Form inputList={inputs} withReCaptcha={true} onSubmit={onSubmit} submitUrl={CONTACT_FORM_SUBMIT_URL}/>
    )   
}

export default ContactForm
