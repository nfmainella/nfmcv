import * as React from 'react';
import SectionBooster from 'shared/section-booster/SectionBooster.container'

import BackgroundImageWrapper from "shared/background-image-wrapper/BackgroundImageWrapper.component";
import ContactForm from "./contact-form/ContactForm.component";
import './Contact.css';

function Contact() {
    return (
        <SectionBooster>
            <div className="Contact">
                <BackgroundImageWrapper
                    imageUrl="https://firebasestorage.googleapis.com/v0/b/nfm-cv.appspot.com/o/contact%2FScreen%20Shot%202020-10-17%20at%2018.03.13.png?alt=media&token=6c63b2a1-1fe0-453c-b640-08ee8b9bcea3"
                    fullWidth={true}
                    noRepeat={true}
                    classModifier="BackgroundImage--smallPadding"
                    height={400}
                    foggyLayerOnTop={true}>
                    <div className="Contact-headerContent">
                        <h1>Thanks for reaching out, i will reply as soon as possible.</h1>
                    </div>
                </BackgroundImageWrapper>
                <ContactForm />
            </div>
        </SectionBooster>
    )
}



export default Contact;
