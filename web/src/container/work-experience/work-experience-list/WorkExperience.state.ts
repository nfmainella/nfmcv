import { WorkExperience } from "./WorkExperience.interface";

import { YearPeriodWithMonth } from "utils/YearPeriodWithMonth";

import toNumber from "lodash/toNumber";
import { YearWithMonth } from "utils/YearWithMonth";
import { WorkExperienceDescription } from "./WorkExperienceContent.interface";

export const workExperienceContentList: WorkExperienceDescription[] = [
    new WorkExperienceDescription(1,
        'The American Express travel site provides a whole platform with a rewards program, that allows American Express customer to book not only individual flights, hotel and cars but packages of these three too.',
        [
            'Maintaining / adding new features to the responsive site, using React, Redux and Ruby for the Middleware.',
            'Maintaining / adding new features to the non responsive part of the site, using AngularJS, VanillaJS and Ruby.',
            'Maintaining / adding new features to the emails (Responsive and non responsive, Ruby, HAML and Foundation for the responsiveness)'],
        [],
        'https://blog.iseatz.com/wp-content/uploads/2018/08/glenn-carstens-peters-203007-unsplash-1200x675.jpg',
        'https://www.iseatz.com/'),
    new WorkExperienceDescription(2,
        `OrderGroove provides an easy solution for retailers, extending their existing website, giving customers the option to subscribe to their favorite product/s in order to get them automatically re purchased monthly. The retailers were provided with a tool to manage the subscriptions, this tool had to be individually adapted to match the retailer's overall site look.`,
        [
            'I worked on the extendable core made in AngularJS, LESS, BabelJS,WebPack, LESS, Lodash, Node. My job was to add / mantain existing / new functionalities to this core',
            `Extedning the core to reach the customer's design/functionality needs`,
        ],
        [],
        'https://media-exp1.licdn.com/dms/image/C4D1BAQE_h0YjSxqD2g/company-background_10000/0?e=2159024400&v=beta&t=rAvxC490p4et_tP4YdBjgwFoVY6Ctrs8XA7iScD2zBo',
        'https://www.ordergroove.com/'),
    new WorkExperienceDescription(3,
        `DonWeb is one of the biggest technolology companies in South America, with their best seller product being hosting, but also providing an array of other services like Domains, Email Marketing, SSL Certificates and Web Sites / Stores.`,
        [
            'Fixing bugs / adding new features to the selling site / landing pages. Mostly vanilla JS and jQuery.',
            'Fixing bugs / adding new features to the system used by customer to manage their products in DonWeb. Mostly vanilla JS and jQuery',
            'Migrating existing management portal from old KnockoutJS to Angular'
        ],
        [],
        'https://firebasestorage.googleapis.com/v0/b/nfm-cv.appspot.com/o/work-experience%2Fbackground%2FDSC05879%20(2).JPG?alt=media&token=7df0f700-a84d-4783-bcf1-8314a14bf27e',
        'https://www.donweb.com/'),
    new WorkExperienceDescription(4,
        `PerroTuerto was a small company based in the city of Rosario, it's main focus was developing games made in java for slot games. It also focused on small Android Games.`,
        [
            'Developing of games in Android 2.2, using technologies like Ardor3D',
            'Fixing bugs / adding new features to the existing slot games made in Java',
            'Small tasks / CMS'
        ],
        [],
        'https://firebasestorage.googleapis.com/v0/b/nfm-cv.appspot.com/o/work-experience%2Fbackground%2FDSC05879%20(2).JPG?alt=media&token=7df0f700-a84d-4783-bcf1-8314a14bf27e',
        'https://www.facebook.com/SomosPerroTuerto'),
    new WorkExperienceDescription(5,
        `Neoris is a global business and IT consulting company that specializes in nearshore outsourcing, value-added consulting, and emerging technologies. Neoris is the largest IT consulting and systems integration company in Mexico and the second in Latin America.`,
        [
            'Work team, analysis and detection of errors',
            'Solutions and improvements for applications, to ensure correct functioning.',
            'Tools used: SQL in Teradata, Application to manage genesis orders (UNIX)'
        ],
        [],
        'https://firebasestorage.googleapis.com/v0/b/nfm-cv.appspot.com/o/work-experience%2Fbackground%2FAbout_us_slider.jpg?alt=media&token=2ad5e081-a267-4988-a698-60466d33bcb8',
        'https://www.neoris.com/'),
    new WorkExperienceDescription(5,
        `T-Mobile US, Inc., is an American wireless network operator; its majority shareholder is the German telecommunications company Deutsche Telekom (DT). Its headquarters are located in Bellevue, Washington, in the Seattle metropolitan area. T-Mobile is the third-largest wireless carrier in the United States.`,
        [
            'Technical support for the popular smartphone operating systems at the time (iOS, Android, Windows Phone, Blackberry, WebOS) and regular smartphones.',
            'Technical support for common tech issues for 2G/3G networks and regular mobile networks.',
            'Customer support with regular accounting, billing issues.'
        ],
        [],
        'https://firebasestorage.googleapis.com/v0/b/nfm-cv.appspot.com/o/work-experience%2Fbackground%2Fslider-bg-03.jpg?alt=media&token=f392b50a-b6ee-4004-bec7-ccf59a8448e6',
        'https://www.neoris.com/')

]

export const workExperienceListState: WorkExperience[] = [
    new WorkExperience(1, "American Express via iSeatz", workExperienceContentList[0].description, "Frontend Developer", "Rosario", "Argentina",
        true, true, new YearPeriodWithMonth(new YearWithMonth(2018, 9)),
        'https://firebasestorage.googleapis.com/v0/b/nfm-cv.appspot.com/o/work-experience%2Ficons%2Fworkexperirence-iseatz-icon.jpg?alt=media&token=0c5e04e9-14e9-4ae1-80a4-507863171cfb',
        workExperienceContentList[0]),
    new WorkExperience(2, "OrderGroove via Endava", workExperienceContentList[1].description, "Frontend Developer", "Rosario", "Argentina", false, false,
        new YearPeriodWithMonth(new YearWithMonth(2018, 7), new YearWithMonth(2018, 9)),
        'https://firebasestorage.googleapis.com/v0/b/nfm-cv.appspot.com/o/work-experience%2Ficons%2Fworkexperience-endva-icon.jpg?alt=media&token=74f5141a-5885-4612-a7b2-afe9e9ac87ff',
        workExperienceContentList[1]),
    new WorkExperience(3, "DonWeb by Web.com", workExperienceContentList[2].description, "Frontend Developer", "Rosario", "Argentina", false, false,
        new YearPeriodWithMonth(new YearWithMonth(2017, 3), new YearWithMonth(2018, 9)),
        'https://firebasestorage.googleapis.com/v0/b/nfm-cv.appspot.com/o/work-experience%2Ficons%2Fworkexperience-endava-icon.jpg?alt=media&token=fc28e825-a432-4310-bdca-6e63a3cd3154',
        workExperienceContentList[2]),
    new WorkExperience(4, "Perro Tuerto for CFyAr", workExperienceContentList[3].description, "Full Stack Developer", "Rosario", "Argentina", false, false,
        new YearPeriodWithMonth(new YearWithMonth(2014, 4), new YearWithMonth(2014, 10)),
        'https://firebasestorage.googleapis.com/v0/b/nfm-cv.appspot.com/o/work-experience%2Ficons%2Fworkexperience-perrotuerto.jpg?alt=media&token=293af375-fdb4-4add-8cb4-3d7ddc5e1c51',
        workExperienceContentList[3]),
    new WorkExperience(5, "Lowe USA via Neoris Argentina", workExperienceContentList[4].description, "Analyst", "Rosario", "Argentina", false, false,
        new YearPeriodWithMonth(new YearWithMonth(2010, 7), new YearWithMonth(2011, 1)),
        'https://firebasestorage.googleapis.com/v0/b/nfm-cv.appspot.com/o/work-experience%2Ficons%2Fworkexperience-neoris-icon.jpg?alt=media&token=8724b64c-b1fa-45fc-9c89-fdbb05b18a53',
        workExperienceContentList[4]),
        new WorkExperience(6, "T-Mobile usa via Teletech", workExperienceContentList[5].description, "Technical Support   ", "Rosario", "Argentina", false, false,
        new YearPeriodWithMonth(new YearWithMonth(2009, 2), new YearWithMonth(2009, 9)),
        'https://firebasestorage.googleapis.com/v0/b/nfm-cv.appspot.com/o/work-experience%2Ficons%2Fworkexperience-teletech-icon.jpg?alt=media&token=d81a0cab-b4e2-4b98-bcc8-be9fc3688b0a',
        workExperienceContentList[5])]

export function findWorkExperience(id: string): WorkExperience {
    const workExperience = workExperienceListState.find(element => element.id === toNumber(id));
    return workExperience ? workExperience : new WorkExperience()
}