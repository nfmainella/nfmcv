import * as React from "react";
import { NavLink } from 'react-router-dom';
import WorkExperienceItem from "../work-experience-list/work-experience-item/WorkExperienceItem";
import { WorkExperience } from "./WorkExperience.interface";
import "./WorkExperienceList.css";

export interface IWorkExperienceListProps {
  elementList: WorkExperience[];
  showAll?: () => void;
  showCurrent?: () => void;
}

function WorkExperienceListView({ elementList: workExperienceList }: IWorkExperienceListProps) {
  const [selectedWorkExperience, setSelectedWorkExperience] = React.useState(0);

  return (
    <div className="WorkExperienceList">
      <div className="WorkExperienceList-content">
        <h1 className="mb-2 ml-2">A brief summary of my work experience</h1>
        {workExperienceList.map(
          (workExperience: WorkExperience, index) => (
            <NavLink to={`work_experience/${workExperience.id}`} className="WorkExperienceList-itemWrapper">
              <WorkExperienceItem workExperience={workExperience} index={index}
                selectedWorkExperience={selectedWorkExperience}
                setSelectedWorkExperience={setSelectedWorkExperience} />
            </NavLink>
          ))}
      </div>
    </div>
  );
}

export default WorkExperienceListView