import {WorkExperienceListAction} from './WorkExperienceList.actions';
import { IWorkExperienceListState } from './WorkExperienceListState.interface';

import { SHOW_ALL, SHOW_CURRENT } from './WorkExperience.constants';
import { workExperienceListState } from './WorkExperience.state';

export function workExperienceListReducer(state: IWorkExperienceListState = {elementList: workExperienceListState}, action: WorkExperienceListAction): IWorkExperienceListState {
  switch (action.type) {
    case SHOW_ALL:
      return {...state, elementList: workExperienceListState};
    case SHOW_CURRENT:
      return {...state, elementList: state.elementList.filter(t => t.current)};
    default:
      return state;
  }
}