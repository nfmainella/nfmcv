import { emptyValueIfUndefined } from 'utils/classUtils';
import { YearPeriodWithMonth } from 'utils/YearPeriodWithMonth'
import { WorkExperienceDescription } from './WorkExperienceContent.interface';
import { WorkExperienceMedia } from './WorkExperienceMedia.interface';

export class WorkExperience {
    public id: number;
    public thumbnailImage?: string;
    public company: string;
    public description: string;
    public position: string;
    public city: string;
    public country: string;
    public remote: boolean;
    public current: boolean = false;
    public yearPeriod?: YearPeriodWithMonth;
    public initialized?: boolean = false;
    public workExperienceDescription: WorkExperienceDescription;
    public workExperienceMedia: WorkExperienceMedia;

    constructor(id?: number, company?: string, description?: string, position?: string, city?: string, country?: string, remote?:boolean, current?: boolean, 
        yearPeriod?: YearPeriodWithMonth, thumbnailImage?: string, workExperienceDescription?: WorkExperienceDescription, workExperienceMedia?: WorkExperienceMedia) {
        this.id = emptyValueIfUndefined(id, 0);
        this.thumbnailImage = emptyValueIfUndefined(thumbnailImage);
        this.company = emptyValueIfUndefined(company);
        this.description = emptyValueIfUndefined(description);
        this.position = emptyValueIfUndefined(position);
        this.city = emptyValueIfUndefined(city);
        this.country = emptyValueIfUndefined(country);
        this.remote = emptyValueIfUndefined(remote, false);
        this.current = emptyValueIfUndefined(current, false);
        this.yearPeriod = emptyValueIfUndefined(yearPeriod, null);
        this.workExperienceDescription = emptyValueIfUndefined(workExperienceDescription, null);
        this.workExperienceMedia = emptyValueIfUndefined(workExperienceMedia, null)
    }
}
