
import * as constants from './WorkExperience.constants';

export interface IShowAll {
    type: constants.SHOW_ALL;
}

export interface IShowCurrent {
    type: constants.SHOW_CURRENT;
}

export type WorkExperienceListAction = IShowAll | IShowCurrent;

export function showAll(): IShowAll {
    return {
        type: constants.SHOW_ALL
    };
}

export function showCurrent(): IShowCurrent {
    return {
        type: constants.SHOW_CURRENT
    };
}