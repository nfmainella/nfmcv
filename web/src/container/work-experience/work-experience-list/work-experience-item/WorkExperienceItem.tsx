import * as React from "react";
import TimelineItem from "shared/timeline-item/TimeLine.component";
import { getDateDistance, getDatePeriod } from "utils/dateUtils";
import { WorkExperience } from "../WorkExperience.interface";
import "./WorkExperienceItem.css";

export const MAX_CHARACTER_DESCRIPTION=190
export interface IWorkItemProps {
    workExperience: WorkExperience;
    isToggled?: boolean;
    index: number;
    selectedWorkExperience: number;
    setSelectedWorkExperience: any;
}

const WorkExperienceItem: React.FunctionComponent<IWorkItemProps> = (props: IWorkItemProps) => {
    const { workExperience } = props;

    return (
        workExperience ?
            <div className="WorkExperienceItem">
                <TimelineItem
                    header={
                        <div className="WorkExperienceItem-contentWrapper">
                            <div className="WorkExperienceItem-top">
                                <p className="WorkExperienceItem-position tt-u">
                                    {workExperience.position}
                                </p>
                                <div className="WorkExperienceItem-companyAndPlace">
                                    <strong className="WorkExperienceItem-company">{workExperience.company} </strong>
                                    {!workExperience.remote && 'in'} <strong className="WorkExperienceItem-location"> {workExperience.remote ? 'Remote work' : `${workExperience.city} - ${workExperience.country}`}</strong>
                                </div> 
                                {workExperience.yearPeriod && <div className="WorkExperienceItem-itemLeftElement">
                                    <em>{workExperience.yearPeriod.endYear ? getDatePeriod(workExperience.yearPeriod) : 'Curent job'}
                                    </em>
                                    <div className="WorkExperienceItem-periodLength">
                                        {getDateDistance(workExperience.yearPeriod)}
                                    </div>
                                </div>
                                }
                            </div>

                        </div>
                    }
                    rightBubble={
                        <img src={workExperience.thumbnailImage} className="WorkExperienceItem-image hidden-sm" />
                    }
                    description={
                        <div className="WorkExperienceItem-contentWrapper max-height">
                            <p className="WorkExperienceItem-description no-margin">
                                {workExperience.workExperienceDescription.description.length > MAX_CHARACTER_DESCRIPTION ?
                                    `${workExperience.workExperienceDescription.description.substring(0, MAX_CHARACTER_DESCRIPTION - 3)}...`
                                    : workExperience.workExperienceDescription.description
                                }
                            </p>
                        </div>

                    }
                />
            </div>
            : <p>Loading</p>
    );
};

export default WorkExperienceItem;
