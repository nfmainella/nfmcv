import {EducationalExperienceListAction} from './EducationalExperienceList.actions';
import { IEducationalExperienceListState } from './EducationalExperienceListState.interface';

import { SHOW_ALL, SHOW_GRADUATED } from './EducationalExperience.constants';
import { EducationalExperience } from './EducationalExperience.interface';
const elementListState: EducationalExperience[] = [
  new EducationalExperience("UAI", 2014, 2018, true, "Bachelor's degree - <strong>System's Engineering</strong>"),
  new EducationalExperience("UNR", 2012, 2014, false, "Systems Analyst"),
  new EducationalExperience("UAI", 2014, 2018, true, "Elementary and high school")  
]
export function educationalExperienceListReducer(state: IEducationalExperienceListState = {elementList: elementListState}, action: EducationalExperienceListAction): IEducationalExperienceListState {
  switch (action.type) {
    case SHOW_ALL:
      return state;
    case SHOW_GRADUATED:
      return {elementList: state.elementList.filter(t => t.graduated)};
    default:
      return state;
  }
}