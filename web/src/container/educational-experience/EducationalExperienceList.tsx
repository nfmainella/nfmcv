import * as React from 'react';
import EducationalExperienceListView from './EducationalExperienceList.container';


export class EducationalExperienceList extends React.Component {
 public render() {
    return (
        <EducationalExperienceListView />
    )  
  }
}

export default EducationalExperienceList;

