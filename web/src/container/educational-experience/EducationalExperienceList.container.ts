import { ICurriculumVitae } from "curriculum-vitae/CurriculumVitae.interface";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import {EducationalExperienceListAction, showAll, showGraduated} from "./EducationalExperienceList.actions";
import EducationalExperienceListView from "./EducationalExperienceList.view";

export function mapStateToProps({ container }: ICurriculumVitae) {
  return {
    elementList: container.educationalExperience.elementList
  };
}

export function mapDispatchToProps(
  dispatch: Dispatch<EducationalExperienceListAction>
) {
  return {
    showAll: () => dispatch(showAll()),
    showGraduated: () => dispatch(showGraduated())
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EducationalExperienceListView);
