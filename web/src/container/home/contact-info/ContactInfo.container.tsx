import { ICurriculumVitae } from "curriculum-vitae/CurriculumVitae.interface";
import { connect } from "react-redux";
import ContactInfoView from "./ContactInfo.view";

export function mapStateToProps({ container }: ICurriculumVitae) {
    return {
        socialIcons: container.contactInfo.socialIcons
    };
}

export default connect(
    mapStateToProps,
)(ContactInfoView);
