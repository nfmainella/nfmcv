import {SocialIcon} from "shared/social-icon/SocialIcon.interface";

export interface IContactInfoState {
    socialIcons: SocialIcon[];
}