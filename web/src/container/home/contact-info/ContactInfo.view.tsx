import * as React from "react";
import {SocialIconComponent} from "shared/social-icon/SocialIcon.component";
import {SocialIcon} from "shared/social-icon/SocialIcon.interface";
import './ContactInfo.css';

export interface IContactInfoProps {
    socialIcons: SocialIcon[];
}

function ContactInfoView({socialIcons}: IContactInfoProps) {
    return (
        <div className="ContactInfo">
            {socialIcons.map(
                (element: SocialIcon) => (
                    <SocialIconComponent url={element.url} icon={`--${element.icon}`}/>
                ))}
        </div>
    );
}

export default ContactInfoView