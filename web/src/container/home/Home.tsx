import * as React from 'react';
import {Button} from 'shared/buttons/Buttons.component';
import {NfmSharedIconsComponent} from 'shared/nfm-shared-icons/NfmSharedIconscomponent';
import SectionBooster from 'shared/section-booster/SectionBooster.container';
import './Home.css';

class Home extends React.Component {
    public render() {
        return (
            <SectionBooster>
                <div className="Home">
                    <div className="Home-container">
                        <div className="Home-wrapper">
                            <h2 className="Home-introduction">Hello there 👋, happy to have you here! My name is <em>Nicolás
                                Mainella</em></h2>
                            <div className="Home-description">But i'm also known as <i>Maine</i>. <br /> I'm a <strong>Senior
                                front end developer</strong>, who has a degree in <strong>system's engineering</strong>, and currently works as a <strong>remote
                                Frontend developer.</strong>
                                <br/>
                                <br/>
                                I am located in Rosario, Argentina. 🇦🇷🇮🇹
                            </div>
                        </div>

                        <div className="Home-learnMore">
                            <Button link="/about_me" classModifier="--small">
                                <div className="Home-learnMoreButton">
                                    <h3 className="Home-learnMoreButtonText tt-u">Learn more about me</h3>
                                    <div className="Home-learnMoreButtonIcon">
                                        <NfmSharedIconsComponent icon="RightArrow" rotate={true}/>
                                    </div>
                                </div>
                            </Button>
                        </div>
                    </div>  

                </div>
            </SectionBooster>
        );
    }
}

export default Home;
