import React, { useCallback, useEffect, useRef } from 'react'
import { IClickOutside } from './IClickOutside.intrerface'

function ClickOutside(props: IClickOutside) {
    const element = useRef(null)
    const clickListener = useCallback(
        (e: MouseEvent) => {
            const notInClickSurface = !(element.current! as any).contains(e.target)
            if (notInClickSurface) {
                props.onClickOutside()
            }
        },
        [element.current],
    )

    useEffect(() => {
        document.addEventListener('click', clickListener)
        return () => {
            document.removeEventListener('click', clickListener)
        }
    }, [])

    return (
        <div ref={element}>
            {props.children}
        </div>
    )
}

export default ClickOutside