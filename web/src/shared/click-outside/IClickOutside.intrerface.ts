
export interface IClickOutside {
    onClickOutside: any;
    onClickInside?: any;
    children: any;
}
