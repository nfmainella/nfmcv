export interface ISectionBooster {
    classModifier?: string;
    whiteFadeBackground?: boolean;
    paddingTop: number;
    children?: any;
    center?: boolean;
}