export interface ITimelineItem {
    header: any;
    children?: any;
    backgroundImage?: string;
    description?: any;
    rightBubble?: any;
    toggleContent?: boolean;
}