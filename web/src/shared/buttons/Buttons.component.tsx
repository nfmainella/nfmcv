import * as React from "react";

import * as classNames from "classnames";
import { NavLink } from "react-router-dom";
import { IButtonProps } from "./Button.interface";
import './Buttons.css';

export function Button(props: IButtonProps) {
    const { target, href, link, onClick, children, classModifier } = props;
    const classNameModifier = classNames('Button', classModifier)

    return (
        href || onClick ? 
        <a 
            className={classNameModifier}
            href={href}
            onClick={onClick}
            target={target ? target : 'default'}
        >
            {children}
        </a>
        : 
        <NavLink to={link} className={classNameModifier} exact={true}>
            {children}
        </NavLink>
    )
}