export interface IButtonProps {
    href?: string;
    link?: any;
    isButton?: boolean;
    onClick?: any;
    target?: string;
    children: any;
    classModifier?: string;
}