import * as classNames from "classnames";
import * as React from "react";
import './Header.css';

export interface IHeaderProps {
    classModifier?: string;
    highlightFirstLetter?: boolean;
}

export default class Header extends React.Component<IHeaderProps> {
    public render() {
        const {classModifier = false} = this.props;
       return (
        <h2 className={classNames('Header ', classModifier)}>{this.props.children}</h2>
       )
    }
}