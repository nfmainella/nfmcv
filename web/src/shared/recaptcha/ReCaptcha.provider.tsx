import React, {createContext, ReactNode, useCallback, useEffect, useMemo, useState} from 'react';
import {injectScript, removeScript} from 'utils/scriptUtils'

export interface IGReCaptchaExecute {  
    // tslint:disable-next-line:ban-types
    execute: Function;
}
interface IReCaptchaProviderProps {
  reCaptchaKey?: string;
  scriptProps: {
    async: boolean;
    id: string;
  };
  children: ReactNode;
}
export interface IReCaptchaConsumerProps {
  executeReCaptcha?: (action?: string) => Promise<string>;
}


 const ReCaptchaContext = createContext<IReCaptchaConsumerProps>({
  executeReCaptcha: () => {
    // This default context function is not supposed to be called
    throw Error(
      'ReCaptcha Context has not yet been implemented, if you are using the hook make sure you are using it inside a context.'
    );
  }
});

function ReCaptchaProvider({
  reCaptchaKey,
  scriptProps,
  children
}: IReCaptchaProviderProps) {
  const scriptId = scriptProps.id;
  const scriptAsync = scriptProps.async;

  const url = `https://www.google.com/recaptcha/api.js?render=${reCaptchaKey}`

  const [greCaptchaInstance, setGreCaptchaInstance] = useState<null | IGReCaptchaExecute>(null);

  const onLoad = () => {
    if (!window || !(window as any).grecaptcha) {
      console.warn(
        `Script not available`
      );

      return;
    }

    const gRecaptcha = (window as any).grecaptcha;

    gRecaptcha.ready(() => {
      setGreCaptchaInstance(gRecaptcha);
    });
  };

  useEffect(() => {
    injectScript(url, scriptId, onLoad, scriptAsync)
  return () => {
      removeScript(scriptId)
    }
  }, [])

  const executeReCaptcha = useCallback(
    async (action: string = 'homepage') => {
      if (!greCaptchaInstance || !greCaptchaInstance.execute) {
        throw new Error(
          'Recaptcha has not been loaded yet'
        );
      }

      const result = await greCaptchaInstance.execute(reCaptchaKey, { action });

      return result;
    },
    [greCaptchaInstance]
  );

  const googleReCaptchaContextValue = useMemo(
    () => ({
      executeReCaptcha: greCaptchaInstance ? executeReCaptcha : undefined
    }),
    [executeReCaptcha, greCaptchaInstance]
  );

  return (
    <>
      <ReCaptchaContext.Provider value={googleReCaptchaContextValue}>
      {children}
    </ReCaptchaContext.Provider>
    </>
  )
}

export {ReCaptchaContext, ReCaptchaProvider};