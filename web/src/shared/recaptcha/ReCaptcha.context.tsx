import {useContext} from 'react'
import {ReCaptchaContext} from './ReCaptcha.provider'

export const getReCaptcha = () => useContext(ReCaptchaContext);

export default getReCaptcha;