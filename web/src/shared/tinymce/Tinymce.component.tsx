import { Editor } from '@tinymce/tinymce-react';
import classnames from 'classnames'
import pick from 'lodash/pick';
import React from 'react';
import './TinyMCE.css';

export interface ITinyMCE {
  height?: number;
  width?: number;
  menuBar?: boolean;
  value: string;
  onEditorChange: any;
  onBlur?: any;
  hasError?: boolean;
}

function TinyMCE(props: ITinyMCE) {
  return (
    <div className={classnames('TinyMCE', {'TinyMCE--error': props.hasError})}>
        <Editor
        {...pick(props, 'value', 'onEditorChange')}
        apiKey="gas7eswk5robwbw6hrnqszaj14sv0ymdx49rqkigxjxnlp2h"
        onBlur={props.onBlur}
        init={{
          height: props.height || "300",
          menubar: props.menuBar ? props.menuBar : false,
          plugins: [
            'advlist autolink lists link image',
            'charmap print preview anchor help',
            'searchreplace visualblocks code',
            'insertdatetime media table paste wordcount'
          ],
          statusbar: false,
          toolbar:
            'undo redo | formatselect | bold italic | \
                alignleft aligncenter alignright | \
                bullist numlist',
          width: props.width || "100%",
        }}
      />
    </div>
  )
}

export default TinyMCE