import * as React from "react";
export interface INfmSharedIconsComponent {
    icon: string;
    rotate: boolean;
}

import * as classNames from "classnames";
import './NfmSharedIcons.css';

export class NfmSharedIconsComponent extends React.Component<INfmSharedIconsComponent> {
    public render() {
        const { icon, rotate = false } = this.props;
        const iconWithClass = icon && `NfmSharedIcons--${icon}`

        return (
            <span className={classNames('NfmSharedIcons ', iconWithClass, {'NfmSharedIcons--rotate90Deg': rotate})}/>
        )
    }
}