import * as React from "react";
import {ISocialIcon} from "./SocialIcon.interface";
import './SocialIcons.css';

export class SocialIconComponent extends React.Component<ISocialIcon,
    object> {

    public render() {
        const { icon, url } = this.props;
        const getClassName = "SocialIconsFont "+icon;
        return (
            <a href={url} className="SocialIcon" target="_blank">
                <span className={getClassName}/>
            </a>
        )
    }
}