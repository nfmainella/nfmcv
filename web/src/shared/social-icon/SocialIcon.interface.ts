export interface ISocialIcon {
    url: string;
    icon: string;
}

export class SocialIcon implements ISocialIcon {
    public icon: string;
    public url: string;

    constructor(icon: string, url: string) {
        this.icon = icon;
        this.url = url;
    }


}