import isEmpty from "lodash/isEmpty";
import {MAX_LENGTH_VALIDATION_MESSAGE, MIN_LENGTH_VALIDATION_MESSAGE, REQUIRED_VALIDATION_MESSAGE} from "./InputValidation.constants"

function wrapWithMessageAndValidity(message: string, condition: boolean) {
    if (condition) {
        return {message};
    } return {}
}

function required(validatingValue: any) {
    return wrapWithMessageAndValidity(
        REQUIRED_VALIDATION_MESSAGE,
        typeof validatingValue === 'string' ? validatingValue.length === 0 : isEmpty(validatingValue)
    )
}

function maxLength(validatingValue: string, maxLengthValue: number) {
    return wrapWithMessageAndValidity(
        MAX_LENGTH_VALIDATION_MESSAGE(maxLengthValue),
        validatingValue.length > maxLengthValue,
    )
}

function minLength(validatingValue: string, minLengthValue: number) {
    return wrapWithMessageAndValidity(
        MIN_LENGTH_VALIDATION_MESSAGE(minLengthValue),
        validatingValue.length < minLengthValue
    )
}

function max(validatingValue: number, maxValue: number) {
    return validatingValue < maxValue
}

function min(validatingValue: number, minValue: number) {
    return validatingValue > minValue
}

export const validationFunctionMapping = {
    MAX: max,
    MAX_LENGTH: maxLength,
    MIN: min,
    MIN_LENGTH: minLength,
    REQUIRED: required
}
   