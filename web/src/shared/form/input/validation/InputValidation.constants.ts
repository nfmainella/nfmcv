export const REQUIRED = 'REQUIRED';
export type REQUIRED = typeof REQUIRED;

export const REQUIRED_VALIDATION_MESSAGE = 'This field is required.';
export type REQUIRED_VALIDATION_MESSAGE = typeof REQUIRED_VALIDATION_MESSAGE;

export let MIN_LENGTH = 'MIN_LENGTH';
export type MIN_LENGTH = typeof MIN_LENGTH;

export const MIN_LENGTH_VALIDATION_MESSAGE = (minLength: number) => `Please, you should at least type ${minLength} characters`;
export type MIN_LENGTH_VALIDATION_MESSAGE = typeof MIN_LENGTH_VALIDATION_MESSAGE;


export const MAX_LENGTH_VALIDATION_MESSAGE = (maxLength: number) => `Please do not exceed the maximum of ${maxLength} characters`;
export type MAX_LENGTH_VALIDATION_MESSAGE = typeof MAX_LENGTH_VALIDATION_MESSAGE;

export const MAX_LENGTH = 'MAX_LENGTH';
export type MAX_LENGTH = typeof MAX_LENGTH;

export const MIN = 'MIN';
export type MIN = typeof MIN;

export const MAX = 'MAX';
export type MAX = typeof MAX;

