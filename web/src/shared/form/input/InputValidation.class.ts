import { validationFunctionMapping } from "shared/form/input/validation/InputValidation";
import { MAX, MAX_LENGTH, MIN_LENGTH, REQUIRED} from "shared/form/input/validation/InputValidation.constants";

export class InputValidation {
    public valid?: boolean;
    public required?: boolean = false;
    public maxLength?: number = 0;
    public minLength?: number = 0;
    public max?: number = 0;
    public min?: number = 0;
    public errorMessages?: string[] = [];

    constructor(required: boolean = false, maxLength: number = 0, minLength: number = 0, max: number = 0, min: number = 0) {
        this.required = required;
        this.maxLength = maxLength;
        this.minLength = minLength;
        this.max = max;
        this.min = min;
        this.errorMessages = [];
    }

    public pushIfDefinedMessage(error: any, currentErrors: string[]) {
        if (error.message) {
            currentErrors.push(error.message)
        }
        return currentErrors
    }


    public validate(value: any) {
        const currentErrors: string[] = [];

        const pushIfDefinedMessage = (error: any) => {
            if (error.message) {
                currentErrors.push(error.message);
            }
        };

        if (this.required) {
            pushIfDefinedMessage(validationFunctionMapping[REQUIRED](value));
        } if (this.maxLength) {
            pushIfDefinedMessage(validationFunctionMapping[MAX_LENGTH](value, this.maxLength));
        } if (this.minLength) {
            pushIfDefinedMessage(validationFunctionMapping[MIN_LENGTH](value, this.minLength));
        } if (this.max) {
            pushIfDefinedMessage(validationFunctionMapping[MAX](value, this.max));
        }
        this.errorMessages = currentErrors;
        this.valid = this.errorMessages.length === 0;
        return this;
    }
}