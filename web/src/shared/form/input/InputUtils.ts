import get from 'lodash/get'
import { Input } from './Input.class'

export function onEditorChangeFn(input: Input, setInput: any, value: any){
    setInput({...input, value, validation: input.validate(value)})
}

export const onBlur = (input: Input) => () => input.onBlur()
export const onChange = (input: Input, setInput: (input: Input) => any) => (e: React.ChangeEvent<HTMLInputElement>): Input => {
    const value = get(e, 'target.value', '')
    const mutatedInput = ({...input, value, validation: input.validate(value)})
    setInput(mutatedInput)
    return mutatedInput
}
export const onEditorChange = (input: Input, setInput: any) => (value: any) => onEditorChangeFn(input, setInput, value)

