import classNames from "classnames";
import React, { useEffect } from "react";
import TinyMCE from 'shared/tinymce/Tinymce.component'
import { Input } from "./Input.class";
import './Input.component.css';
import { InputLabel } from "./InputLabel.class";
import {onBlur, onChange as onChangeFn, onEditorChange} from './InputUtils'

interface IInputComponent {
    input: Input;
}

function InputComponent(props: IInputComponent) {
    const inputWrapper = React.useRef(null);
    const inputReference = React.useRef(null)
    const [inputLabel, setInputLabel] = React.useState(new InputLabel())
    const [input, setInput] = React.useState(props.input)
    
    useEffect(
        () => setInput(props.input),
    [setInput, props.input])

    useEffect(
        () => props.input.onChange(input)
    , [input])

    const hasErrorMessage = React.useMemo(() => {
        return input.validation.errorMessages && input.validation.errorMessages.length > 0
    }, [input.validation.errorMessages])

    const isTextAreaField = React.useMemo(() => {
        return input.type === "textarea"
    }, [input.type])

    const inputProps = {
        // To avoid field auto completion
        autoComplete: 'new-password',
        className: classNames(
            "TextInput-input", 
            { "TextInput-input--bigBottomBorder": inputLabel.top },
            { "TextInput-input--bigBottomError": hasErrorMessage }, 
            input.classModifier
        ),
        name: input.name,
        onBlur: onBlur(input),
        onChange: onChangeFn(input, setInput),
        value: input.value
    }

    const inputLabelOnTop = (_: React.MouseEvent) => {
        setInputLabel({ ...inputLabel, top: true })
    }

    React.useEffect(() => {
        if (inputLabel.top) {
            if (inputReference.current !== null) {
                // @ts-ignore
                inputReference.current.focus()
            }
        }

    }, [inputLabel.top])

    return (
            <>
                <div className="TextInput-inputWrapper" ref={inputWrapper} onClick={inputLabelOnTop}>
                    {
                        input.label && !isTextAreaField &&
                        <label className={classNames('TextInput-label',
                            { 'TextInput-label--top': inputLabel.top },
                            { 'TextInput-label--topNoError': inputLabel.top && !hasErrorMessage },
                            { 'TextInput-label--error': hasErrorMessage })}>
                            {input.label}
                        </label>
                    }
                    {
                        isTextAreaField ?
                            <TinyMCE 
                            value={input.value} 
                            onEditorChange={onEditorChange(input, setInput)} 
                            onBlur={onBlur(input)} 
                            hasError={hasErrorMessage} /> :
                            <input type="text" {...inputProps} ref={inputReference} />
                    }
                </div>

                <div className={classNames('TextInput-errorList', { 'TextInput-errorList--show': hasErrorMessage })}>
                    {
                        input.validation.errorMessages &&
                        input.validation.errorMessages.map(
                            errorMessage =>
                                <div className="TextInput-error" key={errorMessage}>
                                    {errorMessage}
                                </div>
                        )
                    }
                </div>
            </>
    )
}

export default InputComponent

