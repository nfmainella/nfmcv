import noop from 'lodash/noop'
import {InputValidation} from "./InputValidation.class";

export class Input {
    public classModifier?: string;
    public name: string;
    public label: string;
    public onBlur: any;
    public onClick: any;
    public type: string;
    public touched: boolean = false;
    public validation: InputValidation;
    public fullWidth: boolean;
    public value: string;
    public onChange: any;

    constructor(name: string = '', value: string = '', label: string = '',  type: string = 'text', 
        validation: InputValidation = new InputValidation(), onChange: any = noop, 
        fullWidth: boolean = false, onBlur: any = noop, onClick: any = noop) {
        this.name = name;
        this.value = value;
        this.label = label;
        this.onBlur = onBlur;
        this.onClick = onClick;
        this.onChange = onChange;
        this.type = type;
        this.validation = validation;
        this.fullWidth = fullWidth;
    }

    public validate = (value: string = this.value): InputValidation => this.validation.validate(value)

}