import { Input } from "../input/Input.class";

export interface IFormInputOperation {
    inputList: Input[];
    input: Input;
}

export interface IForm {
    classModifier?: string;
    name?: string;
    onSubmit: (valid: boolean, validatedInputs: Input[], reCaptchaToken?: string) => void;
    valid?: boolean;
    inputList: Input[];
    withReCaptcha?: boolean;
    submitUrl: string;
    paramsWhiteList?: string;
}