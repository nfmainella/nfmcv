import * as classNames from "classnames";
import React, {useEffect, useState} from "react";
import { Button } from 'shared/buttons/Buttons.component';
import { Input } from "shared/form/input/Input.class";
import InputComponent from "shared/form/input/Input.component";
import ReCaptchaContext from "shared/recaptcha/ReCaptcha.context";

import {isFormValid, validateForm} from 'utils/formUtils'

import './Form.css';
import { IForm } from "./Form.interface";

function Form(props: IForm) {
    const [valid, setValid] = useState(false)
    const {executeReCaptcha} = ReCaptchaContext()
    
    const composedSubmit = () => {
        const validatedFields = validateForm(props.inputList)
        setValid(isFormValid(validatedFields))
        if (props.withReCaptcha && executeReCaptcha) {
            executeReCaptcha().then(
                (token: string) => {
                    props.onSubmit(valid, validatedFields, token)
                }
            )
        }
        props.onSubmit(valid, validatedFields)
    }

    useEffect(() => {
        setValid(isFormValid(props.inputList))
    }, [props.inputList])

    const form =
    <form className={classNames('Form', props.classModifier)}>
        <div className="Form-inputList">
            {props.inputList && props.inputList.map(
                (input: Input, index: number) => <div className={classNames('Form-input', {'Form-input--fullWidth': input.fullWidth})} key={index}>
                    <InputComponent input={input} />
                </div>
            )}
        </div>
        <Button onClick={composedSubmit} >
            Send
        </Button>
    </form>
    
    return form
}

export default Form