import * as classNames from "classnames";
import * as React from "react";
import "./BackgroundImage.css";

export interface IBackgroundImage {
    children?: any;
    classModifier?: string;
    imageUrl?: string;
    foggyLayerOnTop?: boolean;
    fullHeight?: boolean;
    height?: number;
    fullWidth?: boolean;
    noRepeat?: boolean;
}

const BackgroundImageWrapper = (props: IBackgroundImage) => {
    const { classModifier, foggyLayerOnTop = true, imageUrl, children, height, fullHeight = false, fullWidth = false, noRepeat = false } = props;
    const getImageUrlStyle = `url(${imageUrl}) ${noRepeat && 'no-repeat'}`
    const background = foggyLayerOnTop ? `linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), ${getImageUrlStyle}` : getImageUrlStyle

    return (
        <div className={
            classNames(
                'BackgroundImage', 
                { 'BackgroundImage--fullHeight': fullHeight },
                { 'BackgroundImage--fullWidth': fullWidth }, 
                classModifier
                )
            }
            style={{ background, height: `${height}px` }}>
            {children}
        </div>
    )
}

export default BackgroundImageWrapper