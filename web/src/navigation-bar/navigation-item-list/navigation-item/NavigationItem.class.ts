export class NavigationItemClass {
    public icon: string;
    public title: string;
    public description: string; 
    public path: string;

    constructor(icon: string, title: string, description: string, path: string) {
        this.icon = icon;
        this.title = title;
        this.description = description;
        this.path = path;
    }
}