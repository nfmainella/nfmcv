import { ICurriculumVitae } from 'curriculum-vitae/CurriculumVitae.interface';
import { Dispatch } from 'react';
import { connect } from "react-redux";
import { withRouter } from 'react-router';
import { NavigationBarAction, setTopPosition } from './NavigationBar.actions';
import NavigationBarView from './NavigationBar.view';

export function mapStateToProps({ navigationBar }: ICurriculumVitae) {
  return {
    itemList: navigationBar.itemList
  };
}

export function mapDispatchToProps(
  dispatch: Dispatch<NavigationBarAction>
) {
  return {
    setNavBarTopPosition: (topPaddingAmount: number) => dispatch(setTopPosition({topPaddingAmount}))
  };
}

export const Main = connect(
  mapStateToProps,
  mapDispatchToProps
)(NavigationBarView);

export default withRouter(Main);
