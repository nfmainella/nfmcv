import { NavigationItemClass } from "./navigation-item-list/navigation-item/NavigationItem.class";
export interface INavigationBarPosition {
    topPaddingAmount: number;
    show: boolean;
}

export interface INavigationBar {
    itemList: NavigationItemClass[];
    positioning:  INavigationBarPosition;
}