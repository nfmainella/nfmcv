import { IContainer } from "container/Container.interface";
import { INavigationBar } from "navigation-bar/NavigationBar.interface";

export interface ICurriculumVitae{
    navigationBar: INavigationBar,
    container: IContainer
}