import classNames from 'classnames';
import AboutMe from "container/about-me/AboutMe";
import Contact from "container/contact/Contact.component";
import { EducationalExperienceList } from "container/educational-experience/EducationalExperienceList";
import Home from "container/home/Home";
import WorkExperienceEditView from 'container/work-experience/work-experience-edit/WorkExperienceEditComponent';
import { WorkExperienceList } from "container/work-experience/work-experience-list/WorkExperienceList";
import * as React from "react";
import { HashRouter, Route, Switch } from "react-router-dom";
import {FooterComponent} from "../footer/Footer.component";
import { CurriculumVitae } from "./CurriculumVitae";
import "./CurriculumVitae.css";

export const CurriculumVitaeRouter = () => {
    return (
        <HashRouter>
            <div className={classNames("CurriculumVitae")}>
                <Route component={CurriculumVitae} path="/" />
                <div className="CurriculumVitae-content">
                    <Switch>
                        <Route exact={true} path="/" component={Home} />
                        <Route exact={true} path="/about_me" component={AboutMe} />
                        <Route exact={true} path="/work_experience" component={WorkExperienceList} />
                        <Route exact={true} path="/work_experience/:id" component={WorkExperienceEditView} />
                        <Route exact={true} path="/academic_experience" component={EducationalExperienceList} />
                        <Route exact={true} path="/contact" component={Contact} />
                    </Switch>
                    <FooterComponent/>
                </div>
            </div>
        </HashRouter>
    );
};

