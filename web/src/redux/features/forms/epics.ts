import { RootAction, RootState } from 'MyTypes'
import { Epic } from 'redux-observable';
import {  filter, tap } from 'rxjs/operators';
import { isOfType } from 'typesafe-actions';
import * as constants from './constants'

export const addFormEpic: Epic<
  RootAction,
  RootAction,
  RootState
> = action$ =>
  action$.pipe(
    filter(isOfType(constants.ADD_FORM)), // action is narrowed to: { type: "ADD_TODO"; payload: string; }
    tap(action => {
      logger.log(
        `action type must be equal: ${action.payload}`
      );
    }),
    
  )