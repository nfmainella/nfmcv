import { IForm } from 'shared/form/form/Form.interface';
import { createAction } from 'typesafe-actions';
import * as constants from './constants'

export const addForm = createAction(
    constants.ADD_FORM,
  )<IForm>();

export const removeForm = createAction(constants.REMOVE_FORM)<string>()