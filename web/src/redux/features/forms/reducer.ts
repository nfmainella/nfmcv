import { RootAction, RootState } from 'MyTypes';
import { combineReducers } from 'redux';
import { IForm } from 'shared/form/form/Form.interface';
import { createReducer } from 'typesafe-actions';
import { addForm, removeForm } from './actions';

export const forms: any = createReducer([
    {},
  ] as IForm[])
    // .handleAction(loadTodosAsync.success, (state, action) => action.payload)
    .handleAction(addForm, (state: RootState, action: RootAction) => [...state, action.payload])
    .handleAction(removeForm, (state: RootState, action: RootAction) =>
      state.filter(form => form.name !== action.payload)
    );

const formsReducer = combineReducers(
    forms
)

export default formsReducer