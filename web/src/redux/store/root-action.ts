import * as formActions from '../features/forms/actions';

export default {
  forms: formActions,
};