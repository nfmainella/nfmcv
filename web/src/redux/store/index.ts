import { RootAction, RootState } from 'MyTypes';
import { createStore } from 'redux';
import { createEpicMiddleware } from 'redux-observable';

// import { composeEnhancers } from './utils';

// import rootEpic from './root-epic';
import rootReducer from './root-reducer'
// import services from '../services';

export const epicMiddleware = createEpicMiddleware<
  RootAction,
  RootAction,
  RootState>();
// ({
//   dependencies: services,
// });

// // configure middlewares
// const middlewares = [epicMiddleware];
// // compose enhancers
// const enhancer = composeEnhancers(applyMiddleware(...middlewares));

// rehydrate state on app start
const initialState = {};

// create store
const store = createStore(rootReducer, initialState);

// epicMiddleware.run(rootEpic);

// export store singleton instance
export default store;
