import { combineReducers } from 'redux';

import formsReducer from '../features/forms/reducer';

const rootReducer = combineReducers({
  forms: formsReducer,
});


export default rootReducer;