import { Input } from "shared/form/input/Input.class";
import { InputValidation } from "shared/form/input/InputValidation.class";

export function initContactForm(setName: React.Dispatch<React.SetStateAction<Input>>, setEmail: React.Dispatch<React.SetStateAction<Input>>, setMessage: React.Dispatch<React.SetStateAction<Input>>) {
    setName(new Input('name', '', 'Name', 'text', new InputValidation(true, 50), setName));
    setEmail(new Input('email', '', 'Email', 'text', new InputValidation(true), setEmail));
    setMessage(new Input('message', '', 'Message', 'textarea', new InputValidation(true, 200), setMessage, true));
}