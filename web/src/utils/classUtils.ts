export function emptyValueIfUndefined (value?: any, differentDefault?: any) {
    function stringIfNoDifferentDefault(differentDefaultSetValue?: any) {
        return differentDefaultSetValue ? differentDefaultSetValue : '';
    }
    return value ? value : stringIfNoDifferentDefault(differentDefault);
}