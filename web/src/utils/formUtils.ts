import find from 'lodash/find'
import isUndefined from 'lodash/isUndefined'

import { Input } from "shared/form/input/Input.class";

export function isFormValid(inputList: Input[]): boolean {
    return isUndefined(find(inputList, {valid: false}))
}

export function validateForm(inputList: Input[]): Input[] {
    return inputList.map(
        input => input.touched ? input : {
                ...input, touched: true, validation: input.validate(input.value)
            }
    )
}

// export function sendAjaxWithParams(inputList: Input[], url: string, dispatch: ) {

// }
