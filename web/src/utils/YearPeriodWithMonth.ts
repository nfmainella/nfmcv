import { emptyValueIfUndefined } from 'utils/classUtils';
import { YearWithMonth } from "utils/YearWithMonth";

export class YearPeriodWithMonth {
  public startYear: YearWithMonth;
  public endYear!: YearWithMonth;

  constructor(startYear?: YearWithMonth, endYear?: YearWithMonth) {
      this.startYear = emptyValueIfUndefined(startYear)
      if (endYear) {
        this.endYear = endYear;
      }
  }
}