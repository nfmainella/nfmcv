
const isScriptInjected = (scriptId: string) =>
    !!document.querySelector(`#${scriptId}`);

export function injectScript(scriptUrl: string, scriptId: string, onLoad: () => void, async: boolean) {
    if (isScriptInjected(scriptId)) {
    onLoad();
    return;
    }

    const js = document.createElement('script');

    js.id = scriptId;
    js.src = scriptUrl;

    js.async = !!async;
    js.onload = onLoad;
    document.body.appendChild(js);
}

export function removeScript(scriptId: string) {
    const script = document.querySelector(`#${scriptId}`);
    if (script) {
        script.remove();
    }
}
