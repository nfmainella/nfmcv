import { StateType, ActionType } from 'typesafe-actions';

declare module "MyTypes" {
  export type Store = StateType<typeof import('../src/redux/store/index').default>;
  export type RootAction = ActionType<typeof import('../src/redux/store/root-action').default>;
  export type RootState = StateType<ReturnType<typeof import('../src/redux/store/root-reducer').default>>;
}

declare module 'typesafe-actions' {
  interface Types {
    RootAction: ActionType<typeof import('../src/redux/store/root-action').default>;
  }
}

